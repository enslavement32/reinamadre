package com.example.reinamadre.repository;


import com.example.reinamadre.entity.TipoCita;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TipoCitaRepository extends JpaRepository<TipoCita, Long> {

}