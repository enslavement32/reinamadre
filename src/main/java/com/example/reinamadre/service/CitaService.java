package com.example.reinamadre.service;


import com.example.reinamadre.entity.Cita;
import com.example.reinamadre.entity.TipoCita;
import com.example.reinamadre.repository.CitaRepository;
import com.example.reinamadre.repository.TipoCitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
* Service con logica de negocio
*
*/
@Service
public class CitaService {

    @Autowired
    private CitaRepository citaRepository;
    
    @Autowired
    private TipoCitaRepository tipoCitaRepository;

	/**
	*
	*
	*
	**/
    public Cita findId(Long id){
        return citaRepository.findById(id).orElse(null);
    }
    
    
	/**
	*
	*
	*
	**/
    public List<TipoCita> getTiposCita(){
        return tipoCitaRepository.findAll();
    }
	
	/**
	*
	*
	*
	**/
    public List<Cita> getAllCitas() {
        return citaRepository.findAll();
    }

	/**
	*
	*
	*
	**/
    public Cita createCita(Cita cita) {
        return citaRepository.save(cita);
    }

	/**
	*
	*
	*
	**/
    public Cita updateCita(Cita cita) {
        Cita citaExistente = citaRepository.findById(Long.parseLong(cita.getNumeroCita())).orElse(null);
        citaExistente.setFecha(cita.getFecha());
        citaExistente.setPaciente(cita.getPaciente());
        citaExistente.setTipoCita(cita.getTipoCita());
        citaExistente.setNombreMedico(cita.getNombreMedico());
        return citaRepository.save(citaExistente);
    }

    public void deleteCita(Long id) {
        citaRepository.deleteById(id);
    }
}