package com.example.reinamadre.entity;


import jakarta.persistence.*;

@Entity
@Table(name = "tipocita")
public class TipoCita {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    public TipoCita() {
    }

    public TipoCita(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

  

    @Override
    public String toString() {
        return "TipoCita{" +
                "id=" + id +
                ", nombre=" + nombre +
                '}';
    }
}