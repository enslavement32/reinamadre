package com.example.reinamadre;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Swagger and OpenIA Configuration
 */
@Configuration
public class APISwaggerConfig {
    /**
     * @return OpenAPI Configuration
     */
    @Bean
    public OpenAPI defineOpenApi() {
        Server server = new Server();
        server.setUrl("http://localhost:8091");
        server.setDescription("Java development Test");

        Contact myContact = new Contact();
        myContact.setName("Alejandro Lindarte Castro");
        myContact.setEmail("alejo.lindarte@outlook.com");

        Info information = new Info()
                .title("Examen Tecnico GRM")
                .version("1.0")
                .description("This API exposes endpoints to dates.")
                .contact(myContact);
        return new OpenAPI().info(information).servers(List.of(server));
    }
}
