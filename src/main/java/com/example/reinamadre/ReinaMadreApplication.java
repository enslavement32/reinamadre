package com.example.reinamadre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.jdbc.core.JdbcTemplate;

/**
*
* ReinaMadre Main Class
*
* Se genera el DDL y DML para la base de datos SQLIte
*
**/
@SpringBootApplication
public class ReinaMadreApplication implements CommandLineRunner{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * main method
	 * @param args default params
	 *
	 */
	public static void main(String[] args) {
		SpringApplication.run(ReinaMadreApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS tipocita ("+
    						 "id INTEGER PRIMARY KEY AUTOINCREMENT,"+
    					     "nombre TEXT)");
		
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS cita "+
							 "(id INTEGER PRIMARY KEY AUTOINCREMENT, "+
							 "fecha_hora VARCHAR(100) NOT NULL, "+
							 "paciente VARCHAR(100) NOT NULL, "+
							 "tipo_cita INTEGER NOT NULL, "+
							 "medico VARCHAR(100) NOT NULL, "+
							 "numero_cita INTEGER NOT NULL, "+
							 "estado CHAR(1) NOT NULL, "+
							 "CHECK (estado IN('A','I')), "+
							 "FOREIGN KEY (tipo_cita) REFERENCES tipocita(id) )");


		jdbcTemplate.execute("INSERT INTO tipocita (nombre) VALUES ('Consulta')");
		jdbcTemplate.execute("INSERT INTO tipocita (nombre) VALUES ('Servicio')");
		jdbcTemplate.execute("INSERT INTO tipocita (nombre) VALUES ('Tratamiento')");
		
		jdbcTemplate.execute("INSERT INTO cita (fecha_hora, paciente, tipo_cita, medico, numero_cita, estado) VALUES ('2024/05/31', 'Alejo', 1,'Fred',1, 'A')");

	}


}
