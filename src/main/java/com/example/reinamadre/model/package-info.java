/**
 * 'Organizacion vigilancia' models
 * <p>
 * Package with the 'Organizacion vigilancia' dto's
 *
 * DeviceMessage
 * Messageresponse
 *
 * </p>
 * @author Alejandro Lindarte castro
 * @version 1.0
 */

package com.example.reinamadre.model;