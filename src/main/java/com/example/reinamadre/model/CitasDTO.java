package com.example.reinamadre.model;

public class CitasDTO {
	
    private Long id;

    private String fecha;

    private String paciente;

    private String tipoCita;

    private String nombreMedico;

    private String numeroCita;
	
    private String estado;
	
	public CitasDTO() 
	{
    }

    public CitasDTO(String fecha, String paciente, String tipoCita, String nombreMedico, String numeroCita, String estado) {
        this.fecha = fecha;
        this.paciente = paciente;
        this.tipoCita = tipoCita;
        this.nombreMedico = nombreMedico;
        this.numeroCita = numeroCita;
		this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getTipoCita() {
        return tipoCita;
    }

    public void setTipoCita(String tipoCita) {
        this.tipoCita = tipoCita;
    }

    public String getNombreMedico() {
        return nombreMedico;
    }

    public void setNombreMedico(String nombreMedico) {
        this.nombreMedico = nombreMedico;
    }

    public String getNumeroCita() {
        return numeroCita;
    }

    public void setNumeroCita(String numeroCita) {
        this.numeroCita = numeroCita;
    }
	
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "CitaDTO{" +
                "id=" + id +
                ", fecha=" + fecha +
                ", paciente='" + paciente + '\'' +
                ", tipoCita='" + tipoCita + '\'' +
                ", nombreMedico='" + nombreMedico + '\'' +
                ", numeroCita='" + numeroCita + '\'' +
			    ", estado='" + estado + '\'' +
                '}';
    }
}
