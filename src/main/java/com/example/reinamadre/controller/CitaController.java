package com.example.reinamadre.controller;

import com.example.reinamadre.entity.Cita;
import com.example.reinamadre.entity.TipoCita;
import com.example.reinamadre.service.CitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/citas")
public class CitaController {

    @Autowired
    private CitaService citaService;

    @GetMapping("/all")
    public List<Cita> getAll() {
        return citaService.getAllCitas();
    }

    @GetMapping("/{id}")
    public Cita getById(@PathVariable Long id) {
        return citaService.findId(id);
    }

    @PostMapping("/create")
    public Cita create(@RequestBody Cita cita) {
        return citaService.createCita(cita);
    }

    @PutMapping("/{id}")
    public Cita update(@PathVariable Long id, @RequestBody Cita cita) {
        return citaService.updateCita(cita);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        citaService.deleteCita(id);
        return ResponseEntity.noContent().build();
    }
    
    @GetMapping("/tipocita/all")
    public List<TipoCita> getAllTipos() {
        return citaService.getTiposCita();
    }


    /**
     *
     * @return the correct message builed
     */
    @GetMapping("/")
    public String default_api() {
        return "Reina madre API version 1.0";
    }

}